<?php
namespace App\Schemas;

use App\Traits\ApiRequest;
use GuzzleHttp\Client;
use FlixTech\SchemaRegistryApi\Registry\PromisingRegistry;
use FlixTech\SchemaRegistryApi\Exception\SchemaRegistryException;

class NewSchemas {

   use ApiRequest;

   public function schemaRegistry(){
        $subject = "local-subject";
        $registry = new PromisingRegistry(
            new Client(['base_uri' => 'localhost:8081'])
        );
        
        // Register a schema with a subject
        $schema = \AvroSchema::parse('{"type": "string"}');
        
        $promise = $registry->register($subject, $schema);
        
        $promise = $promise->then(
            static function ($schemaIdOrSchemaRegistryException) {
                if ($schemaIdOrSchemaRegistryException instanceof SchemaRegistryException) {
                    throw $schemaIdOrSchemaRegistryException;
                }
                
                return $schemaIdOrSchemaRegistryException;
            }
        );
        
        // Resolve the promise
        $schemaId = $promise->wait();
        
        // Get a schema by schema id
        $promise = $registry->schemaForId($schemaId);
        // As above you could add additional callbacks to the promise
        $schema = $promise->wait();
        
        // Get the version of a schema for a given subject.
        $version = $registry->schemaVersion(
            $subject,
            $schema
        )->wait();

        
        // You can also get a schema by subject and version
        $schema = $registry->schemaForSubjectAndVersion($subject, $version)->wait();

        // You can additionally just query for the currently latest schema within a subject.
        $latestSchema = $registry->latestVersion($subject)->wait();

        return $latestSchema;
        
        // Sometimes you want to find out the global schema id for a given schema
        // $schemaId = $registry->schemaId('test-subject', $schema)->wait();
    }

    public function getSchemaLatest($subject)
    {
        $schemaHostSubject = "localhost:8081/subjects";
        return $this->requestHttp("GET", "{$schemaHostSubject}/{$subject}/versions/latest");
    }

    public function getSchemaByID($id)
    {
        $schemaHost = "localhost:8081/schemas/ids/{$id}";
        return $this->requestHttp("GET", $schemaHost);
    }
}
