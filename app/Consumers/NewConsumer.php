<?php

function newConsumer(){
    $conf = new \RdKafka\Conf();
    $conf->set('bootstrap.servers', 'localhost:9092');
    $conf->set('group.id', 'test');
    $conf->set('log_level', (string) LOG_DEBUG);
    $conf->set('debug', 'all');
    $conf->setLogCb(
        function (\RdKafka\Consumer $consumer, int $level, string $facility, string $message): void {
            // Perform your logging mechanism here
        }
    );

    $conf->set('statistics.interval.ms', (string) 1000);
    $conf->setStatsCb(
        function (\RdKafka\Consumer $consumer, string $json, int $jsonLength, $opaque): void {
            // Perform your stats mechanism here ...
        }
    );
}