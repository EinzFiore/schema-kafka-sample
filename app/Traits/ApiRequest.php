<?php

namespace App\Traits;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

trait ApiRequest
{
    public function requestHttp($method, $endpoint, $data = [])
    {
        // $token = request()->header('authorization');
        // Using curl
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $endpoint,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POSTFIELDS => http_build_query($data),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                // "Authorization: {$token}",
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            // dd(json_decode($response));
            return json_decode($response);
        }
    }
}
