<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Schemas\NewSchemas;
use Illuminate\Support\Facades\Log;

class GetSchema extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'run:schema';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Retrive schema kafka';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->schema = new NewSchemas;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        print_r($this->schema->getSchemaLatest("local-topic-value"));
    }
}
