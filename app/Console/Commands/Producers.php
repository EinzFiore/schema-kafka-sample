<?php

namespace App\Console\Commands;

use App\Producers\NewProducers;
use Illuminate\Console\Command;

class Producers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'run:producers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Running kafka producers sample';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->prod = new NewProducers;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        return $this->prod->newProd();
    }
}
