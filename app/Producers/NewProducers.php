<?php
namespace App\Producers;

use Monolog\Logger;
use Monolog\Handler\StdoutHandler;

class NewProducers {

   public function prodConf(){
        $conf = new \RdKafka\Conf();
        $conf->set('bootstrap.servers', 'localhost:9092');
        $conf->set('socket.timeout.ms', (string) 50);
        $conf->set('log_level', (string) LOG_DEBUG);
        $conf->set('debug', 'all');

        return $conf;
    }

    public function confTopic(){
        $topicConf = new \RdKafka\TopicConf();
        $topicConf->set('message.timeout.ms', (string) 30000);
        $topicConf->set('request.required.acks', (string) -1);
        $topicConf->set('request.timeout.ms', (string) 5000);

        return $topicConf;
    }
    
    public function newProd(){
        $producer = new \RdKafka\Producer($this->prodConf());
        // $producer->addBrokers("localhost:9092");

        $topic = $producer->newTopic('local-topic', $this->confTopic());
        
        for ($i = 0; $i < 100; $i++) {
            $key = $i % 10;
            $payload = sprintf('payload-%d-%s', $i, $key);
            $topic->produce(RD_KAFKA_PARTITION_UA, 0, $payload, (string) $key);
        
            // trigger callback queues
            $producer->poll(1);
        }
        
        $producer->flush(5000);
    }
}

